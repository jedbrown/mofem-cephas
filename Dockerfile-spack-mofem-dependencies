FROM       ubuntu:18.10
MAINTAINER Lukasz.Kaczmarczyk@glasgow.ac.uk

# general environment for docker
ENV        DEBIAN_FRONTEND=noninteractive \
           SPACK_ROOT=/usr/local \
           FORCE_UNSAFE_CONFIGURE=1

# install minimal spack dependencies
RUN        apt-get update \
           && apt-get install -y --no-install-recommends \
              autoconf \
              build-essential \
              ca-certificates \
              coreutils \
              curl \
              environment-modules \
              git \
              python \
              unzip \
              vim \ 
              gfortran \
              openssh-server \
           && rm -rf /var/lib/apt/lists/*

# load spack environment on login
RUN        echo "source $SPACK_ROOT/share/spack/setup-env.sh" \
           > /etc/profile.d/spack.sh

# spack settings
# note: if you wish to change default settings, add files alongside
#       the Dockerfile with your desired settings. Then uncomment this line
#COPY       packages.yaml modules.yaml $SPACK_ROOT/etc/spack/

# install spack
RUN        curl -s -L https://api.github.com/repos/likask/spack/tarball/mofem \
           | tar xzC $SPACK_ROOT --strip 1
# note: at this point one could also run ``spack bootstrap`` to avoid
#       parts of the long apt-get install list above

# install software
RUN        spack install --only dependencies mofem-cephas \
           && spack clean -a

# image run hook: the -l will make sure /etc/profile environments are loaded
CMD        /bin/bash -l
